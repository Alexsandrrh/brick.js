"use strict"

let Brick = {};

(function (b) {
    let version = 'v1.0.1';
    // view Brick.js version
    b.version = function () {
        console.info(String('Brick.js version: ' + version));
    };

    b.component = {};

    // renderDOM - this function insert html or xml code to html page
    b.renderDOM = function(_parent, _thing, _add) {
        let parentDOM = document.querySelector(_parent);
        if (_add === false || _add === undefined) {
            insertHtml(parentDOM, checkValueComponent(_thing));
        } else {
            insertAppendHtml(parentDOM, checkValueComponent(_thing));
        }
        setNewAttr(_parent, _thing);
    };

    // checkValueComponent === checking value in function
    function checkValueComponent(_thing) {
        if (_thing === undefined) {
            console.error('Brick.js => Component doesn\'t exist');
            return 'ERR';
        } else {
            return _thing;
        }
    }

    // insertHTML === innerHTML = html
    function insertHtml(_parent, _child) {
        if (typeof _child === "function") {
            _parent.innerHTML = _child();
        } else {
            _parent.innerHTML = _child;
        }
    }

    // insertAppendHTML === innerHTML += html
    function insertAppendHtml(_parent, _child) {
        if (typeof _child === "function") {
            _parent.innerHTML += _child();
        } else {
            _parent.innerHTML += _child;
        }
    }

    // setNewAttr - set parent, child new attributes
    function setNewAttr(_parent) {
        let parentDOM = document.querySelector(_parent);
        let parentDOMList = parentDOM.children;
        parentDOM.setAttribute('brick-parent-status', true);
        for (var i = 0; i < parentDOMList.length; i++) {
            parentDOMList[i].setAttribute('brick-child-id', i);
        }
        checkChildrenParent(parentDOMList.length, parentDOM, parentDOMList)
    }

    function checkChildrenParent(sum, parentDOM, parentDOMList) {
        if (sum >= 4) {
            parentDOM.setAttribute('brick-sum-elements', parentDOMList.length);
        }
    }

    // front-end size display
    b.displaySize = function (_size, _value, _func) {
        if (_size <= _value) {
            _func();
        }
    };

    // delete element
    b.deleteElement = function (_name) {
        let element = document.querySelector(_name);
        element.remove();
    };

    // insert svg symbols
    function insertInHead(item) {
        let headTag = document.querySelector('head');
        headTag.innerHTML += item;
    }

    b.toInsertSvg = function (link) {
        insertInHead('<svg xmlns="http://www.w3.org/2000/svg" class="visually-hidden"><use xlink:href="' + link + '"></use></svg>');
        toInsertStyle('.visually-hidden {\n' +
            '    border: 0!important;\n' +
            '    clip: rect(0 0 0 0)!important;\n' +
            '    width: 1px!important;\n' +
            '    height: 1px!important;\n' +
            '    margin: -1px!important;\n' +
            '    overflow: hidden!important;\n' +
            '    padding: 0!important;\n' +
            '    position: absolute!important;')
    };

    function toInsertStyle(code) {
        insertInHead('<style brick-elem="true">' + code + '</style>');
    }

})(Brick);

function render(_code, _module) {
    checkModulesInRender(_module);
    return _code;
}

function checkModulesInRender(_module) {
    if (_module === undefined || _module === true) {
        return true;
    } else {
        return false;
    }
}